import Vue from 'vue';
import Router from 'vue-router';
import TableView from './components/mainContent/TableView.vue';
import VidgetView from './components/mainContent/VidgetView.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      name: 'TableView',
      component: TableView,
    },
    {
      path: '*',
      name: 'VidgetView',
      component: VidgetView,
    },
  ],
});

export default router;
