import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export const store = new Vuex.Store({
  state: {
    cards: [],
    orderType: null,
    sortBy: null,
    theme: '',
  },
  actions: {
    formatData(ctx, arr) {
      const obj = JSON.parse(JSON.stringify(arr));
      const ruCategories = new Map([
        ['Pickup', 'Самовывоз'],
        ['Delivery', 'Курьерская доставка'],
        ['Pick-point', 'Доставка в пункт выдачи'],
      ]);
      let date = null;
      let formatDate = null;
      for (let i = 0; i < obj.length; i++) {
        if (ruCategories.has(obj[i].type)) {
          obj[i].type = ruCategories.get(obj[i].type);
        }
        date = new Date(Date.parse(obj[i].creationDate));
        formatDate = `${
          date.getDate().toString().length === 2
            ? date.getDate().toString()
            : `0${date.getDate().toString()}`
        }.${
          (date.getMonth() + 1).toString().length === 2
            ? (date.getMonth() + 1).toString()
            : `0${(date.getMonth() + 1).toString()}`
        }.${date.getFullYear().toString()} ${
          date.getHours().toString().length === 2
            ? date.getHours().toString()
            : `0${date.getHours().toString()}`
        }:${
          (parseInt(date.getMinutes() / 5, 10) * 5).toString().length === 2
            ? (parseInt(date.getMinutes() / 5, 10) * 5).toString()
            : `0${(parseInt(date.getMinutes() / 5, 10) * 5).toString()}`
        }:00`;
        obj[i].creationDate = formatDate;
      }
      ctx.commit('updateCard', obj);
    },
    setStoreType(ctx, data) {
      ctx.commit('updateType', data);
    },
    sortCards(ctx, type) {
      if (type === 'increase') {
        ctx.commit('sortCardsByIncrease');
      } else {
        ctx.commit('sortCardsByDecrease');
      }
    },
    setTheme(ctx, theme) {
      ctx.commit('updateTheme', theme);
    },
    removeCardElement(ctx, id) {
      const filtredArr = ctx.state.cards.filter((card) => card.id !== id);
      ctx.commit('updateCard', filtredArr);
    },
  },
  mutations: {
    updateCard(state, data) {
      state.cards = data;
    },
    updateType(state, data) {
      state.orderType = data;
    },
    sortCardsByIncrease(state) {
      state.cards = state.cards.sort((a, b) => b.number.localeCompare(a.number));
    },
    sortCardsByDecrease(state) {
      state.cards = state.cards.sort((a, b) => a.number.localeCompare(b.number));
    },
    updateTheme(state, data) {
      state.theme = data;
    },
  },
  getters: {
    cards: (state) => state.cards,
    cardsByTypes: (state) => state.cards.filter((card) => (state.orderType ? card.type.toLowerCase().replace(/\s/g, '') === state.orderType.toLowerCase().replace(/\s/g, '') : false)),
  },
});
